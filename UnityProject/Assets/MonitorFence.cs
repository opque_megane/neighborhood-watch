﻿using UnityEngine;
using System.Collections;

public class MonitorFence : MonoBehaviour {

    public static MonitorFence instance;

    int xDiv = 3;
    int zDiv = 3;

    public int nScreens 
    {
        get {return 2 * (xDiv + zDiv);}
    }

    public GameObject monitorPrefab;
	// Use this for initialization
	void Awake () {
        instance = this;

    //assume "monitor" is world unit square
        float tubeRadius = 9;



        float monitorWidth = 5;
        float monitorHeight = 3;







        for (int i = 0; i < xDiv; i++)
        {
         
            Vector3[] oppSideOffsets = {Vector3.zero, Vector3.forward * zDiv * monitorWidth};

            foreach (Vector3 offset in oppSideOffsets)
            {
            GameObject newMonitor = GameObject.Instantiate(monitorPrefab);
            newMonitor.transform.parent = this.transform;


            //newMonitor.transform.up = this.transform.up;
                newMonitor.transform.localPosition =  Vector3.right * ((i + .5f) * monitorWidth) + offset;
            //newMonitor.transform.forward =  this.transform.rotation * radiusVec;
            newMonitor.transform.localEulerAngles = new Vector3(0, 0, 0);
                newMonitor.transform.localScale = new Vector3(monitorWidth, monitorHeight,1);
            }

        }

        for (int i = 0; i < xDiv; i++)
        {

            Vector3[] oppSideOffsets = {Vector3.zero, Vector3.right * xDiv * monitorWidth};

            foreach (Vector3 offset in oppSideOffsets)
            {
                GameObject newMonitor = GameObject.Instantiate(monitorPrefab);
                newMonitor.transform.parent = this.transform;


                //newMonitor.transform.up = this.transform.up;
                newMonitor.transform.localPosition =  Vector3.forward * ((i + .5f) * monitorWidth) + offset;
                //newMonitor.transform.forward =  this.transform.rotation * radiusVec;
                newMonitor.transform.localEulerAngles = new Vector3(0, 90, 0);
                newMonitor.transform.localScale = new Vector3(monitorWidth,monitorHeight,1);
            }

        }

      

    
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public SimpleCameraMonitor getMonitor(int index)
    {
        return this.transform.GetChild(index).GetComponent<SimpleCameraMonitor>();
    }


}
