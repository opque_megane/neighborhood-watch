﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SimpleCameraMonitor : MonoBehaviour {
  
        public RenderTexture rt;
    public GameObject worldLabelPrefab;
    static int number  =0;
    int myNumber = -1;

        void Start ()
        {


        myNumber = number;
        number ++;

        if (worldLabelPrefab != null)
        {
            GameObject worldLabel = GameObject.Instantiate(worldLabelPrefab);
            //worldLabel.transform.parent = this.transform;
            worldLabel.transform.rotation = this.transform.rotation;
            worldLabel.transform.position = this.transform.position + this.transform.forward * .02f;//Vector3.zero + Vector3.forward * .02f;
            worldLabel.transform.localScale = Vector3.one * 3.5f;
            worldLabel.GetComponentInChildren<Text>().text = "x" + myNumber;
        }

        



            rt =  new RenderTexture(256,256,16);
            rt.Create();
            rt.name = "" + myNumber;

            this.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = rt;
            Material outMat = this.transform.GetChild(0).GetComponent<Renderer>().material;

            foreach(Renderer r in this.GetComponentsInChildren<Renderer>())
            {
            
                r .material = outMat;
               
            }     

            //Create another camera, that doesn't move, and renders to the same texture.
        }

    void Update()
    {

    }


    //Only sort of worked. maybe still useful
    /*public void OnGUI ()
    {

        //Vector3 savedScale = this.transform.localScale;
        //this.transform.localScale = Vector3.one;
        RenderTexture prevRt =RenderTexture.active;
        RenderTexture.active = rt;
        AlexUtil.DrawCenteredText( Vector2.zero, "-" + myNumber + "-", 120, Color.red, "");
        RenderTexture.active = prevRt;
        //this.transform.localScale = savedScale;
    } */



        


}
