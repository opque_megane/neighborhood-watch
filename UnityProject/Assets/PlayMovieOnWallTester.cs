﻿using UnityEngine;
using System.Collections;

public class PlayMovieOnWallTester : MonoBehaviour {

    public MovieTexture movie;


	// Use this for initialization
	void Start () {
        //this.GetComponent<Renderer>()
        movie.loop = true;

        movie.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
