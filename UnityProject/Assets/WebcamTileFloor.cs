﻿using UnityEngine;
using System.Collections;

public class WebcamTileFloor : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach (Renderer r  in this.GetComponentsInChildren<Renderer>())
        {
            //r.material.mainTexture = WebcamManager.instance.getWebcamTexture(0);
            r.material.SetTexture("_MainTex", WebcamManager.instance.getWebcamTexture(0));
            r.material.SetTexture("_Tex2", WebcamManager.instance.getWebcamTexture(1));
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
