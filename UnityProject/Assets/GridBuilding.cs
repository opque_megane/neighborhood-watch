﻿using UnityEngine;
using System.Collections;

using WallType = GridBuildingWall.WallType;
public class GridBuilding : MonoBehaviour {

	// Use this for initialization
	void Start () {
        shuffleWalls();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            shuffleWalls();
        }
	}

    public void shuffleWalls()
    {
        WallType[] allWallTypes = {WallType.Solid, WallType.Perforated, WallType.Empty};

        foreach(GridBuildingWall wall in this.GetComponentsInChildren<GridBuildingWall>())
        {
            int idx = Random.Range(0, (int) allWallTypes.Length);
            int diceRoll = Random.Range(0, 100);
            if (diceRoll < 20)
            {
                idx = 2;
            }
            else if (diceRoll < 60)
            {
                idx = 1;
            }
            else
            {
                idx = 0;
            }


            WallType randType = allWallTypes[idx];
            wall.setWallType(randType);
        }
    }
}
