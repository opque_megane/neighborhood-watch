﻿using UnityEngine;
using System.Collections;

public class SetFromWebcam : MonoBehaviour {

    public int camIdx;
	// Use this for initialization
    void Start () {
        this.GetComponent<Renderer>().material.mainTexture = WebcamManager.instance.getWebcamTexture(camIdx);
	}	
	// Update is called once per frame
	void Update () {
	
	}
}
