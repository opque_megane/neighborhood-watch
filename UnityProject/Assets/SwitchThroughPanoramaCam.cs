﻿using UnityEngine;
using System.Collections;

public class SwitchThroughPanoramaCam : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Transform getCameraTransform(int idx)
    {
        return this.transform.FindChild("Camera"+idx);
    }

    public void setCamerasOn(bool on)
    {
        foreach(Camera c in this.GetComponentsInChildren<Camera>())
        {
            c.enabled = on;
        }
    }

}
