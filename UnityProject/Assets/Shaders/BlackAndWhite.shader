﻿/*

--- UNUSED ----
Note: was replaced by MaskAlphaWithTexture.shader

A shader that makes a hole in a spirite, or other texture object.

Used in scenario 1 to make the hole the strings background
render texture.

in the frag() call below, you can see that the shader just checks
if that pixel's texture coordinates are inside the ellipse,
and sets the alpha to zero, if so, (or reduces if close to the edge
for soft 'feathered effect')

*/

Shader "MeinShader/BlackAndWhite"
{
   Properties
   {
      _MainTex ("Base (RGB)", 2D) = "white" {}
      _Ellipse ("Ellipse x,y,w,h", Vector) = (.5,.5,.5,.5) //in UV coords
	  _FeatherAmt ("Feather amount", Range(.01,3)) = 0
		  _Color ("Tint Color", Color) = (1,1,1,1)
      
   }
     SubShader
     {
         Tags 
         { 
             "RenderType" = "Transparent" 
             "Queue" = "Transparent" 
         }
 
         Pass
         {
             ZWrite Off
             Blend SrcAlpha OneMinusSrcAlpha 
  
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma multi_compile DUMMY PIXELSNAP_ON
  
             sampler2D _MainTex;
			   uniform float4 _MainTex_ST; 
             float4 _Color;
		

 
             struct Vertex
             {
                 float4 vertex : POSITION;
                 float2 uv_MainTex : TEXCOORD0;
  
             };
     
             struct Fragment
             {
                 float4 vertex : POSITION;
                 float2 uv_MainTex : TEXCOORD0;
                

             };
  
             Fragment vert(Vertex v)
             {
                 Fragment o;
     
                 o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                 o.uv_MainTex = v.uv_MainTex;
				 
				 o.uv_MainTex =  v.uv_MainTex.xy * _MainTex_ST.xy + _MainTex_ST.zw;

                 return o;
             }
                                                     
             float4 frag(Fragment IN) : COLOR
             {
                 float4 o = float4(1, 0, 0, 0.2);
			

				 half4 c = tex2D (_MainTex, IN.uv_MainTex);
				 float averagedColors = (c.r + c.g + c.b) / 3;
				 c.rgb = averagedColors * _Color;

				 o.rgba = c.rgba;
     
                     
                 return o;
             }
 
             ENDCG
         }
     }
 }