﻿    Shader "XU/CombineTexturesWithMask" {
        Properties {
            _MainTex ("Base (RGB)", 2D) = "white" {}
            _Tex2 ("Base (RGB)", 2D) = "white" {}
            _AlphaTex ("Alpha (A)", 2D) = "white" {}
            //_Flip ("InvertAlpha", bool) = false

        }
SubShader {
    Tags {"RenderType"="Opaque" "IgnoreProjector"="True"}
LOD 100

ZWrite On
Blend SrcAlpha OneMinusSrcAlpha

Pass {  
    CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag

        #include "UnityCG.cginc"

        struct appdata_t {
            float4 vertex : POSITION;
            float2 uv : TEXCOORD0;
            //float2 uv2 : TEXCOORD1;
            //float2 texcoordAlpha : TEXCOORD2;
        };

        struct v2f {
            float4 vertex : SV_POSITION;
            half2 uv : TEXCOORD0;
            half2 uv2 : TEXCOORD1;
            half2 uvAlpha : TEXCOORD2;
			//half2 texcoord2 : TEXCOORD1;
            //half2 texcoordAlpha : TEXCOORD2;
    
        };

        sampler2D _MainTex;

        sampler2D _Tex2;
        float4 _Tex2_ST;

        sampler2D _AlphaTex;
        float4 _AlphaTex_ST;

        float4 _MainTex_ST;

        v2f vert (appdata_t v)
        {
            v2f o;
            o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
            o.uv = TRANSFORM_TEX(v.uv, _MainTex);
            o.uv2 = v.uv * _Tex2_ST.xy + _Tex2_ST.zw;
            o.uvAlpha = v.uv * _AlphaTex_ST.xy + _AlphaTex_ST.zw;
            return o;
        }

        fixed4 frag (v2f i) : SV_Target
        {

        		i.uv[0] = i.uv[0] - floor(i.uv[0]) ;
				i.uv[1] = i.uv[1] - floor(i.uv[1]) ;


            i.uv2[0] = i.uv2[0] - floor(i.uv2[0]) ;
				i.uv2[1] = i.uv2[1] - floor(i.uv2[1]) ;

			fixed4 col = tex2D(_MainTex, i.uv);
            fixed4 col2 = tex2D(_Tex2, i.uv2);
            fixed alf = tex2D(_AlphaTex, i.uvAlpha).a;

            col *= alf;
            col.a = 1;
           	col2.rgb *= 1-alf;
           	col += col2;

            //Uncomment for only alpha of mask
            //float alpha = col2.r+_alphaValue;
            //return fixed4(col.r, col.g, col.b,alpha);
             return col;//fixed4(col.r, col.g, col.b,_alphaValue);
        }
    ENDCG
}
}
}
