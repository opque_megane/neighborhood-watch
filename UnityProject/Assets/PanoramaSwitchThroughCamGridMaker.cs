﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PanoramaSwitchThroughCamGridMaker : MonoBehaviour {

    struct IntPair
    {
        public int i;
        public int j;
        public IntPair(int i, int j)
        {
            this.i = i;
            this.j = j;
        }
    }

    public float width = 40;
    public int rows = 2;
    int columns;
    public GameObject panCamPrefab;

    IntPair currentCamIdx = new IntPair(0,0);
    Transform currentCam;
    List<Transform> allPanCams = new List<Transform>();
    //Dictionary<IntPair, Transform> cameraGridIndices = new Dictionary<IntPair, Transform>();

    public bool adjustWithTerrainHeight = false;



	// Use this for initialization
	void Start () {
        float height = width;
        columns = rows;

        int nCams = columns * rows * 4;//4 camers in each panorama;

        int cameraI = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                float r = ((0.0f + i) / rows);
                float c = ((0.0f + j) / columns);

                r -= .5f;
                c -= .5f;
                Vector3 spawnPos = this.transform.position 
                    + r * this.transform.forward * height
                    + c * this.transform.right * width;

                spawnPos += .1f * Random.insideUnitSphere;

                if (adjustWithTerrainHeight)
                {
                    RaycastHit hitInfo;
                    Physics.Raycast(new Ray(spawnPos + Vector3.up * 100, -Vector3.up), out hitInfo, float.PositiveInfinity, LayerMask.NameToLayer("terrain"));

                    spawnPos.y = hitInfo.point.y;
                }


                GameObject panoCam = (GameObject) GameObject.Instantiate(panCamPrefab, spawnPos, Quaternion.identity);
                panoCam.transform.parent = this.transform;

                allPanCams.Add(panoCam.transform);

            }
        }
	}

    //void moveToNextCamera (Vector3 Direction)
    //{
        
    //}

    void updateCameraChoice(IntPair newChoice)
    {
        getCamera(this.currentCamIdx).GetComponent<SwitchThroughPanoramaCam>().setCamerasOn(false);
        getCamera(newChoice).GetComponent<SwitchThroughPanoramaCam>().setCamerasOn(true);
        this.currentCamIdx = newChoice;
        
    }

    Transform getCamera(IntPair coords)
    {
        return this.allPanCams[(coords.i * columns) + coords.j];
    }

	
	// Update is called once per frame
	void Update () {

        bool needToUpdate = false;

        IntPair newChoice = new IntPair(currentCamIdx.i, currentCamIdx.j);
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            newChoice.i = (currentCamIdx.i + 1) % this.rows;
            needToUpdate = true;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            newChoice.i = (currentCamIdx.i - 1 + this.rows) % this.rows;
            needToUpdate = true;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            newChoice.j = (currentCamIdx.j + 1) % this.columns;
            needToUpdate = true;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            newChoice.j = (currentCamIdx.j - 1 + this.columns) % this.columns;
            needToUpdate = true;
        }

        if (needToUpdate)
        {
            updateCameraChoice(newChoice);
        }
	
	}


    void OnDrawGizmos()
    {
        if (rows != 0 && columns != 0)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(getCamera(currentCamIdx).transform.position, .5f);
        }
    }



}
