﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraZone : MonoBehaviour {

    int nCameras = 72;

 

    public GameObject screen;
    RenderTexture renderTexture;
    public Transform cameraSpawnPt;

    public GameObject cameraPrefab;

    List<Camera> camerasInZone;
    public Transform boxArea;

    void Awake()
    {

        camerasInZone = new List<Camera>();
        renderTexture = new RenderTexture(1024, 512, 16);
        renderTexture.Create();

        screen.GetComponent<Renderer>().material.mainTexture = renderTexture;
        
        //Make the monitor
    }

    IEnumerator cameraSpawn()
    {

        int xDiv = Mathf.CeilToInt(Mathf.Sqrt(nCameras));
        int yDiv = Mathf.CeilToInt(nCameras / xDiv);

        Vector2 viewPortSplitSize = new Vector2(1.0f / xDiv, 1.0f /yDiv);

        for (int i = 0; i < nCameras; i++)
        {
            GameObject newCamera = 
                (GameObject) GameObject.Instantiate(
                    cameraPrefab, 
                    cameraSpawnPt.position + Random.insideUnitSphere * 1, 
                    Random.rotationUniform);

            cameraPrefab.GetComponent<ReturnToPositionBeyondVerticalLimit>().respawnPoint = cameraSpawnPt;


            //newCamera.transform.localScale = Vector3.Scale(newCamera.transform.localScale, (Vector3.one + Random.insideUnitSphere * 2.5f));

            newCamera.transform.parent = this.transform;
            yield return new WaitForSeconds(.5f);


            newCamera.GetComponent<Camera>().targetTexture = renderTexture;


            //designate speficic point on the render texture

            int screenXi = i % xDiv;
            int screenYi = i / xDiv;

            newCamera.GetComponent<Camera>().rect = new Rect( Vector2.Scale(new Vector2(screenXi, screenYi), viewPortSplitSize), viewPortSplitSize);


            Vector3 lowerLeft = screen.transform.position - .5f *
                (
                    screen.transform.localScale.x * screen.transform.right 
                    +
                    screen.transform.localScale.y * screen.transform.up 
                );

            Vector3 cellSize = screen.transform.localScale;
            cellSize.x /= xDiv;
            cellSize.y /= yDiv;

            Vector3 cellPos = lowerLeft 
                + (screenXi + .5f) * cellSize.x * screen.transform.right 
                + (screenYi + .5f) * cellSize.y * screen.transform.up
                -  .02f * screen.transform.forward; 

            print("new text");
            newCamera.GetComponent<ObeyCameraZoneRules>().positionMonitorText(
                //lowerLeft + new Vector3 
                cellPos
                ,
                screen.transform.rotation,
                Vector3.one * 3
            );

            //TODO : 
            //Add a text overlay as well to a camera cell
        }
    }

	// Use this for initialization
	void Start () {
        StartCoroutine(cameraSpawn());
	}
	
	// Update is called once per frame
	void Update () 
    {
 
        UpdateContainedCameras();
        //TileContainedCamerasUpdate();
	}


    void UpdateContainedCameras()
    {
        camerasInZone.Clear();
        Collider[] inBox = Physics.OverlapBox(boxArea.transform.position, .5f * boxArea.transform.localScale, boxArea.rotation);

        foreach(Collider c in inBox)
        {

            if (c.GetComponentInParent<Camera>() != null)
            {
                camerasInZone.Add(c.GetComponentInParent<Camera>());

            }
        }
    }


    void TileContainedCamerasUpdate()
    {
        int nCamsInZone = camerasInZone.Count;

        if (nCamsInZone > 0)
        {
            int yDiv = Mathf.CeilToInt(Mathf.Sqrt(nCamsInZone));
            int xDiv = Mathf.CeilToInt(nCamsInZone / yDiv);

            Vector2 viewPortSplitSize = new Vector2(1.0f / xDiv, 1.0f /yDiv);





            int i = 0;
            foreach(Camera cam in this.GetComponentsInChildren<Camera>())
            {

                if (camerasInZone.Contains(cam))
                {
                    cam.enabled = true;
                    int screenXi = i % xDiv;
                    int screenYi = i / xDiv;

                    cam.rect = new Rect( Vector2.Scale(new Vector2(screenXi, screenYi), viewPortSplitSize), viewPortSplitSize);

                    i++;
                }
                else
                {
                    cam.enabled = false;
                }
            }
        }
    }
}
