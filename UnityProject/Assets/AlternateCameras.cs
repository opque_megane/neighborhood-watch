﻿using UnityEngine;
using System.Collections;
using Vectrosity;

public class AlternateCameras : MonoBehaviour {
    public Camera cam1;
    public Camera cam2;
	// Use this for initialization
	void Start () {
        //cam1.enabled = true;
        //cam2.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        //cam1.enabled = !cam1.enabled;
        //cam2.enabled = !cam2.enabled;

        cam2.enabled = false;
        VectorLine.SetCamera3D(cam1);
        CameraOutline.redraw("overall_cam");
        cam2.enabled = true;
    }

    void LateUpdate () {

        cam1.enabled = false;
        VectorLine.SetCamera3D(cam2);
        CameraOutline.redraw("pov_cam");
        cam1.enabled = true;
	}
}
