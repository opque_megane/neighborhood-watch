﻿using UnityEngine;
using System.Collections;

public class PanoramaCamGridMaker : MonoBehaviour {

    public float width = 40;
    public int rows = 2;
    public GameObject panCamPrefab;
    public GameObject screen;
    public RenderTextureScreen ezScreen;

    RenderTexture renderTexture;

    public bool adjustWithTerrainHeight = false;

	// Use this for initialization
	void Start () {
        float height = width;
        int columns = rows;

        renderTexture = new RenderTexture(1024 , 2048, 16);
        renderTexture.Create();

        int nCams = columns * rows * 4;//4 camers in each panorama;

        //int nRowsScreenCells = (int)Mathf.Floor(Mathf.Sqrt(nCams));
        //int nColsScreenCells = (int)Mathf.Ceil(nCams /nRowsScreenCells);



        //screen.GetComponent<Renderer>().material.mainTexture = renderTexture;

        ezScreen.setup(columns * rows, 4);

        int cameraI = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                float r = ((0.0f + i) / rows);
                float c = ((0.0f + j) / columns);

                r -= .5f;
                c -= .5f;
                Vector3 spawnPos = this.transform.position 
                    + r * this.transform.forward * height
                    + c * this.transform.right * width;


                if (adjustWithTerrainHeight)
                {
                    RaycastHit hitInfo;
                    Physics.Raycast(new Ray(spawnPos + Vector3.up * 100, -Vector3.up), out hitInfo, float.PositiveInfinity, LayerMask.NameToLayer("terrain"));
                
                    spawnPos.y = hitInfo.point.y;
                }

                GameObject panoCam = (GameObject) GameObject.Instantiate(panCamPrefab, spawnPos, Quaternion.identity);
                panoCam.transform.parent = this.transform;

                foreach(Camera cam in panoCam.GetComponentsInChildren<Camera>())
                {
                    /*cam.targetTexture = renderTexture;

                    float screenXi = cameraI % nRowsScreenCells;
                    float screenYi = ((int)(cameraI / nRowsScreenCells));

                    cam.rect = new Rect( 
                        new Vector2(screenXi/nColsScreenCells, screenYi/nRowsScreenCells), 
                        new Vector2(1.0f / nColsScreenCells, 1.0f / nRowsScreenCells));*/
                    ezScreen.assignCamera(cam, cameraI);
                    cameraI++;
                }


            }
        }
       
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
