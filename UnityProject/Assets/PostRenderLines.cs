﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PostRenderLines : MonoBehaviour {
    public Material lineMaterial;
    public GameObject followedThing;
    public static PostRenderLines instance;

    public List<Vector3> ptsToRender;

    void Awake()
    {
        instance = this;
        ptsToRender = new List<Vector3>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
    Vector3 mousePos;

	// Update is called once per frame
	void Update () {
        //mousePos = followedThing.transform.position;//Input.mousePosition;
	}

    void OnPostRender()
    {
        //yield return new WaitForEndOfFrame();
        if (!lineMaterial) {
            Debug.LogError("Please Assign a material on the inspector");
            return;
        }
        GL.PushMatrix();
        lineMaterial.SetPass(0);
        //GL.LoadOrtho();//draw in screen space, w/o draws in world coordinates
        foreach(Vector3 v in ptsToRender)
        {
            GL.Begin(GL.LINES);
        GL.Color(Color.red);
            GL.Vertex(v);
        //GL.Vertex(Vector3.zero);
        //GL.Vertex(mousePos);//new Vector3(mousePos.x / Screen.width, mousePos.y / Screen.height, 0));
        GL.End();

        }
        GL.PopMatrix();
    }


}
