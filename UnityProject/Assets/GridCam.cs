﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class GridCam : MonoBehaviour {
    VectorLine vl;
    float camHeight = 1.5f;
    public GameObject camModel;
    public Transform viewPt;
	// Use this for initialization
	void Start () {
        vl = new VectorLine("pole", new List<Vector3>(){Vector3.zero, Vector3.up * camHeight}, 1);
        vl.material = GlobalMaterials.instance.gridHouseLineMaterial;
        vl.drawTransform = this.transform;
        vl.Draw3DAuto();
        camModel.transform.localPosition = Vector3.up * camHeight;
	
	}
	
	// Update is called once per frame
	void Update () {
        camModel.transform.localEulerAngles = Vector3.up * Time.time * 5; 
	
	}
}
