﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity; 

public class CameraOutline : MonoBehaviour {

    static int counter = 0;
    public Material mat;
    public Camera mainCam;

    static List<VectorLine> allLines;

    static List<Vector3> pts = null;
    public VectorLine myLine;
	// Use this for initialization
	void Start () {


        if (allLines == null)
        {
            allLines = new List<VectorLine>();
        }
        if (pts == null)
        {
            pts = new List<Vector3>(){new Vector3(1.799f, 0.473f, 0f), new Vector3(1.799f, 0.473f, 2.998f), new Vector3(0f, 0.473f, 2.998f), new Vector3(1.799f, 0.473f, 2.998f), new Vector3(0f, 0.473f, 2.998f), new Vector3(0f, 0.473f, 0f), new Vector3(1.799f, 0.473f, 0f), new Vector3(0f, 0.473f, 0f), new Vector3(0f, 0.473f, 0f), new Vector3(0f, -0.426f, 0f), new Vector3(0f, -0.426f, 2.998f), new Vector3(0f, -0.426f, 0f), new Vector3(0f, -0.426f, 2.998f), new Vector3(1.799f, -0.426f, 2.998f), new Vector3(1.799f, -0.426f, 0f), new Vector3(1.799f, -0.426f, 2.998f), new Vector3(1.799f, 0.473f, 0f), new Vector3(1.799f, -0.426f, 0f), new Vector3(0f, -0.426f, 0f), new Vector3(1.799f, -0.426f, 0f), new Vector3(1.799f, 0.473f, 2.998f), new Vector3(1.799f, -0.426f, 2.998f), new Vector3(0f, -0.426f, 2.998f), new Vector3(0f, 0.473f, 2.998f), new Vector3(0.849f, 0.405f, 2.998f), new Vector3(0.95f, 0.405f, 2.998f), new Vector3(0.849f, 0.405f, 2.998f), new Vector3(0.752f, 0.379f, 2.998f), new Vector3(0.665f, 0.329f, 2.998f), new Vector3(0.752f, 0.379f, 2.998f), new Vector3(0.665f, 0.329f, 2.998f), new Vector3(0.594f, 0.258f, 2.998f), new Vector3(0.544f, 0.171f, 2.998f), new Vector3(0.594f, 0.258f, 2.998f), new Vector3(0.544f, 0.171f, 2.998f), new Vector3(0.518f, 0.074f, 2.998f), new Vector3(0.518f, -0.027f, 2.998f), new Vector3(0.518f, 0.074f, 2.998f), new Vector3(0.518f, -0.027f, 2.998f), new Vector3(0.544f, -0.123f, 2.998f), new Vector3(0.594f, -0.21f, 2.998f), new Vector3(0.544f, -0.123f, 2.998f), new Vector3(0.594f, -0.21f, 2.998f), new Vector3(0.665f, -0.281f, 2.998f), new Vector3(0.665f, -0.281f, 2.998f), new Vector3(0.752f, -0.331f, 2.998f), new Vector3(0.752f, -0.331f, 2.998f), new Vector3(0.849f, -0.357f, 2.998f), new Vector3(0.849f, -0.357f, 2.998f), new Vector3(0.95f, -0.357f, 2.998f), new Vector3(1.046f, -0.331f, 2.998f), new Vector3(0.95f, -0.357f, 2.998f), new Vector3(1.046f, -0.331f, 2.998f), new Vector3(1.133f, -0.281f, 2.998f), new Vector3(1.133f, -0.281f, 2.998f), new Vector3(1.204f, -0.21f, 2.998f), new Vector3(1.204f, -0.21f, 2.998f), new Vector3(1.255f, -0.123f, 2.998f), new Vector3(1.255f, -0.123f, 2.998f), new Vector3(1.28f, -0.027f, 2.998f), new Vector3(1.28f, 0.074f, 2.998f), new Vector3(1.28f, -0.027f, 2.998f), new Vector3(1.28f, 0.074f, 2.998f), new Vector3(1.255f, 0.171f, 2.998f), new Vector3(1.255f, 0.171f, 2.998f), new Vector3(1.204f, 0.258f, 2.998f), new Vector3(1.133f, 0.329f, 2.998f), new Vector3(1.204f, 0.258f, 2.998f), new Vector3(1.133f, 0.329f, 2.998f), new Vector3(1.046f, 0.379f, 2.998f), new Vector3(0.95f, 0.405f, 2.998f), new Vector3(1.046f, 0.379f, 2.998f), new Vector3(0.799f, 0.398f, 3.643f), new Vector3(0.899f, 0.411f, 3.643f), new Vector3(0.706f, 0.359f, 3.643f), new Vector3(0.799f, 0.398f, 3.643f), new Vector3(0.706f, 0.359f, 3.643f), new Vector3(0.625f, 0.298f, 3.643f), new Vector3(0.625f, 0.298f, 3.643f), new Vector3(0.564f, 0.218f, 3.643f), new Vector3(0.525f, 0.124f, 3.643f), new Vector3(0.564f, 0.218f, 3.643f), new Vector3(0.525f, 0.124f, 3.643f), new Vector3(0.512f, 0.024f, 3.643f), new Vector3(0.525f, -0.077f, 3.643f), new Vector3(0.512f, 0.024f, 3.643f), new Vector3(0.525f, -0.077f, 3.643f), new Vector3(0.564f, -0.17f, 3.643f), new Vector3(0.625f, -0.25f, 3.643f), new Vector3(0.564f, -0.17f, 3.643f), new Vector3(0.625f, -0.25f, 3.643f), new Vector3(0.706f, -0.312f, 3.643f), new Vector3(0.799f, -0.351f, 3.643f), new Vector3(0.706f, -0.312f, 3.643f), new Vector3(0.799f, -0.351f, 3.643f), new Vector3(0.899f, -0.364f, 3.643f), new Vector3(1f, -0.351f, 3.643f), new Vector3(0.899f, -0.364f, 3.643f), new Vector3(1.093f, -0.312f, 3.643f), new Vector3(1f, -0.351f, 3.643f), new Vector3(1.174f, -0.25f, 3.643f), new Vector3(1.093f, -0.312f, 3.643f), new Vector3(1.235f, -0.17f, 3.643f), new Vector3(1.174f, -0.25f, 3.643f), new Vector3(1.235f, -0.17f, 3.643f), new Vector3(1.274f, -0.077f, 3.643f), new Vector3(1.274f, -0.077f, 3.643f), new Vector3(1.287f, 0.024f, 3.643f), new Vector3(1.274f, 0.124f, 3.643f), new Vector3(1.287f, 0.024f, 3.643f), new Vector3(1.274f, 0.124f, 3.643f), new Vector3(1.235f, 0.218f, 3.643f), new Vector3(1.174f, 0.298f, 3.643f), new Vector3(1.235f, 0.218f, 3.643f), new Vector3(1.174f, 0.298f, 3.643f), new Vector3(1.093f, 0.359f, 3.643f), new Vector3(1.093f, 0.359f, 3.643f), new Vector3(1f, 0.398f, 3.643f), new Vector3(0.899f, 0.411f, 3.643f), new Vector3(1f, 0.398f, 3.643f), new Vector3(0.899f, 0.337f, 3.643f), new Vector3(0.818f, 0.326f, 3.643f), new Vector3(0.743f, 0.295f, 3.643f), new Vector3(0.818f, 0.326f, 3.643f), new Vector3(0.678f, 0.245f, 3.643f), new Vector3(0.743f, 0.295f, 3.643f), new Vector3(0.678f, 0.245f, 3.643f), new Vector3(0.628f, 0.18f, 3.643f), new Vector3(0.628f, 0.18f, 3.643f), new Vector3(0.597f, 0.105f, 3.643f), new Vector3(0.587f, 0.024f, 3.643f), new Vector3(0.597f, 0.105f, 3.643f), new Vector3(0.597f, -0.057f, 3.643f), new Vector3(0.587f, 0.024f, 3.643f), new Vector3(0.597f, -0.057f, 3.643f), new Vector3(0.628f, -0.133f, 3.643f), new Vector3(0.628f, -0.133f, 3.643f), new Vector3(0.678f, -0.198f, 3.643f), new Vector3(0.743f, -0.247f, 3.643f), new Vector3(0.678f, -0.198f, 3.643f), new Vector3(0.743f, -0.247f, 3.643f), new Vector3(0.818f, -0.279f, 3.643f), new Vector3(0.899f, -0.289f, 3.643f), new Vector3(0.818f, -0.279f, 3.643f), new Vector3(0.98f, -0.279f, 3.643f), new Vector3(0.899f, -0.289f, 3.643f), new Vector3(0.98f, -0.279f, 3.643f), new Vector3(1.056f, -0.247f, 3.643f), new Vector3(1.121f, -0.198f, 3.643f), new Vector3(1.056f, -0.247f, 3.643f), new Vector3(1.121f, -0.198f, 3.643f), new Vector3(1.17f, -0.133f, 3.643f), new Vector3(1.202f, -0.057f, 3.643f), new Vector3(1.17f, -0.133f, 3.643f), new Vector3(1.202f, -0.057f, 3.643f), new Vector3(1.212f, 0.024f, 3.643f), new Vector3(1.202f, 0.105f, 3.643f), new Vector3(1.212f, 0.024f, 3.643f), new Vector3(1.202f, 0.105f, 3.643f), new Vector3(1.17f, 0.18f, 3.643f), new Vector3(1.17f, 0.18f, 3.643f), new Vector3(1.121f, 0.245f, 3.643f), new Vector3(1.056f, 0.295f, 3.643f), new Vector3(1.121f, 0.245f, 3.643f), new Vector3(1.056f, 0.295f, 3.643f), new Vector3(0.98f, 0.326f, 3.643f), new Vector3(0.899f, 0.337f, 3.643f), new Vector3(0.98f, 0.326f, 3.643f)};
        for (int i = 0; i < pts.Count; i++)
        {
            pts[i] = pts[i] / 3.944739f;
        }
        }

        myLine = new VectorLine("CamLines_" + counter, pts, 1.0f);//, LineType.Points, Joins.Weld);
        //myLine.Draw3DAuto();
        myLine.drawTransform = this.transform;
        myLine.material = mat;
        myLine.SetWidth(1);

        //myLine.layer = this.gameObject.layer;

        allLines.Add(myLine);

        counter++;
	
	}
    public static void redraw(string layer)
    {
        int layerInt = LayerMask.NameToLayer(layer);
        foreach(VectorLine vl in allLines)
        {
            vl.layer = layerInt;
            vl.Draw3D();
        }
    }
	// Update is called once per frame
	void Update () {
	
	}
}
