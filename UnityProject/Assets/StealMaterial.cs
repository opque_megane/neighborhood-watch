﻿using UnityEngine;
using System.Collections;

public class StealMaterial : MonoBehaviour {

    public Renderer stealFrom;
    public string objectName = "";

	// Use this for initialization
	void Start () {

        if (objectName != "")
        {
            stealFrom  =  GameObject.Find(objectName).GetComponent<Renderer>();//.GetComponent<Renderer>().material;
        }

        this.GetComponentInChildren<Renderer>().material = this.stealFrom.material;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
