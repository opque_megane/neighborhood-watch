﻿using UnityEngine;
using System.Collections;
using Vectrosity;

public class OnPrerender : MonoBehaviour {
    public Camera otherCam;
    public string specialLayer = "";
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnPreRender()
    {
        //print(this.gameObject.name);
        VectorLine.SetCamera3D(this.GetComponent<Camera>());
        CameraOutline.redraw(specialLayer);
    }

    void OnPostRender()
    {
        VectorLine.SetCamera3D(otherCam);//Camera.main);

        CameraOutline.redraw(specialLayer);
    }
}
