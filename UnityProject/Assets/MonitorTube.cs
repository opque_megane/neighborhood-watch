﻿using UnityEngine;
using System.Collections;

public class MonitorTube : MonoBehaviour {

    public static MonitorTube instance;

    public GameObject monitorPrefab;
    public int nDivisions = 16;
	// Use this for initialization
	void Awake () {
        instance = this;

 

    //assume "monitor" is world unit square
        float tubeRadius = 9;



        float monitorWidth = 2 * tubeRadius * Mathf.Tan(Mathf.PI / nDivisions);

        for (int i = 0; i < nDivisions; i++)
        {
            float angle = (0.0f + i) / nDivisions * 2 * Mathf.PI;
            float angleDegrees = (360.0f * i) / nDivisions;

            GameObject newMonitor = GameObject.Instantiate(monitorPrefab);
            newMonitor.transform.parent = this.transform;

            Vector3 radiusVec = new Vector3(Mathf.Sin(angle),0,Mathf.Cos(angle));
            //radiusVec = this.transform.rotation * radiusVec;


            //newMonitor.transform.up = this.transform.up;
            newMonitor.transform.localPosition =  radiusVec * tubeRadius;
            //newMonitor.transform.forward =  this.transform.rotation * radiusVec;
            newMonitor.transform.localEulerAngles = new Vector3(0, angleDegrees, 0);
            newMonitor.transform.localScale = new Vector3(monitorWidth,3,1);



        }
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public SimpleCameraMonitor getMonitor(int index)
    {
        return this.transform.GetChild(index).GetComponent<SimpleCameraMonitor>();
    }


}
