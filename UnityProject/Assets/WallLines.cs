﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity; 

public class WallLines : MonoBehaviour {
    public Material mat;
    public Camera mainCam;
    List<Vector3> pts = new List<Vector3>(){new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 3.24f, -1.44f), new Vector3(0f, 3.24f, -1.44f), new Vector3(0f, 3.24f, -2.88f), new Vector3(0f, 3.24f, -2.88f), new Vector3(0f, 2.34f, -2.88f), new Vector3(0f, 2.34f, -2.88f), new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 2.34f, 1.44f), new Vector3(-0.638f, 0f, 1.44f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 2.34f, -1.44f), new Vector3(-0.638f, 0f, -1.44f), new Vector3(-0.638f, 0f, -1.44f), new Vector3(-0.638f, 0f, 1.44f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 3.24f, 1.44f), new Vector3(0f, 3.24f, 1.44f), new Vector3(0f, 3.24f, -1.44f), new Vector3(0f, 3.24f, 2.88f), new Vector3(0f, 3.24f, 1.44f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 2.34f, 2.88f), new Vector3(0f, 2.34f, 2.88f), new Vector3(0f, 3.24f, 2.88f), new Vector3(0f, 0f, 2.88f), new Vector3(0f, 2.34f, 2.88f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 0f, 1.44f), new Vector3(0f, 0f, 1.44f), new Vector3(0f, 0f, 2.88f), new Vector3(0f, 0f, -1.44f), new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 2.34f, -2.88f), new Vector3(0f, 0f, -2.88f), new Vector3(0f, 0f, -2.88f), new Vector3(0f, 0f, -1.44f), new Vector3(0f, 0f, 1.44f), new Vector3(-0.638f, 0f, 1.44f), new Vector3(0f, 0f, -1.44f), new Vector3(-0.638f, 0f, -1.44f), new Vector3(0f, 0f, -1.44f), new Vector3(0f, 0f, 1.44f)};
    //{new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 3.24f, -1.44f), new Vector3(0f, 3.24f, -1.44f), new Vector3(0f, 3.24f, -2.88f), new Vector3(0f, 3.24f, -2.88f), new Vector3(0f, 2.34f, -2.88f), new Vector3(0f, 2.34f, -2.88f), new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 0f, 1.44f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 0f, 1.44f), new Vector3(0f, 0f, 2.88f), new Vector3(0f, 0f, 2.88f), new Vector3(0f, 2.34f, 2.88f), new Vector3(0f, 2.34f, 2.88f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 2.34f, -1.44f), new Vector3(0f, 0f, -1.44f), new Vector3(0f, 0f, -1.44f), new Vector3(0f, 0f, 1.44f), new Vector3(0f, 2.34f, 1.44f), new Vector3(0f, 3.24f, 1.44f), new Vector3(0f, 3.24f, 1.44f), new Vector3(0f, 3.24f, -1.44f), new Vector3(0f, 3.24f, 2.88f), new Vector3(0f, 3.24f, 1.44f), new Vector3(0f, 2.34f, 2.88f), new Vector3(0f, 3.24f, 2.88f), new Vector3(0f, 2.34f, -2.88f), new Vector3(0f, 0f, -2.88f), new Vector3(0f, 0f, -2.88f), new Vector3(0f, 0f, -1.44f)};
	// Use this for initialization
	void Start () {
        //VectorLine.SetLine3D(Color.black, pts);
        VectorLine myLine = new VectorLine("Line", pts, 1.0f);//, LineType.Points, Joins.Weld);
        myLine.Draw3DAuto();
        myLine.drawTransform = this.transform;
        myLine.material = mat;
        myLine.SetWidth(2);
        VectorLine.SetCanvasCamera (mainCam);//Camera.main);

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}


