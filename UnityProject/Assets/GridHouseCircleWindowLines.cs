﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class GridHouseCircleWindowLines : MonoBehaviour {
    // Use this for initialization

    VectorLine myLine;

	void Awake () 
    {
        List<Vector3> circleLines2 = new List<Vector3>(){new Vector3(0f, 2.862f, 2.02f), new Vector3(0f, 2.88f, 2.16f), new Vector3(0f, 2.862f, 2.02f), new Vector3(0f, 2.808f, 1.89f), new Vector3(0f, 2.722f, 1.778f), new Vector3(0f, 2.808f, 1.89f), new Vector3(0f, 2.61f, 1.692f), new Vector3(0f, 2.722f, 1.778f), new Vector3(0f, 2.61f, 1.692f), new Vector3(0f, 2.48f, 1.638f), new Vector3(0f, 2.48f, 1.638f), new Vector3(0f, 2.34f, 1.62f), new Vector3(0f, 2.34f, 1.62f), new Vector3(0f, 2.2f, 1.638f), new Vector3(0f, 2.07f, 1.692f), new Vector3(0f, 2.2f, 1.638f), new Vector3(0f, 2.07f, 1.692f), new Vector3(0f, 1.958f, 1.778f), new Vector3(0f, 1.872f, 1.89f), new Vector3(0f, 1.958f, 1.778f), new Vector3(0f, 1.818f, 2.02f), new Vector3(0f, 1.872f, 1.89f), new Vector3(0f, 1.818f, 2.02f), new Vector3(0f, 1.8f, 2.16f), new Vector3(0f, 1.8f, 2.16f), new Vector3(0f, 1.818f, 2.3f), new Vector3(0f, 1.872f, 2.43f), new Vector3(0f, 1.818f, 2.3f), new Vector3(0f, 1.958f, 2.542f), new Vector3(0f, 1.872f, 2.43f), new Vector3(0f, 1.958f, 2.542f), new Vector3(0f, 2.07f, 2.628f), new Vector3(0f, 2.2f, 2.682f), new Vector3(0f, 2.07f, 2.628f), new Vector3(0f, 2.34f, 2.7f), new Vector3(0f, 2.2f, 2.682f), new Vector3(0f, 2.48f, 2.682f), new Vector3(0f, 2.34f, 2.7f), new Vector3(0f, 2.48f, 2.682f), new Vector3(0f, 2.61f, 2.628f), new Vector3(0f, 2.722f, 2.542f), new Vector3(0f, 2.61f, 2.628f), new Vector3(0f, 2.722f, 2.542f), new Vector3(0f, 2.808f, 2.43f), new Vector3(0f, 2.862f, 2.3f), new Vector3(0f, 2.808f, 2.43f), new Vector3(0f, 2.88f, 2.16f), new Vector3(0f, 2.862f, 2.3f)};
        List<Vector3> circleLines = new List<Vector3>();
     

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                Vector3 offset= Vector3.forward * (-1.44f * i) + Vector3.up * (-1.44f * j); 

                foreach(Vector3 v in circleLines2)
                {
                    circleLines.Add(v + offset);
                }
            }
        }

        myLine = new VectorLine("Circle1", circleLines, 4.0f);//, LineType.Points, Joins.Weld);
        myLine.drawTransform = this.transform;
        //myLine.material = mat;
        myLine.SetWidth(2);
        //myLine.color = Color.black;
        myLine.material = GlobalMaterials.instance.gridHouseLineMaterial;
        myLine.Draw3DAuto();

     
	
	}
	
	// Update is called once per frame
	void Update () {

        if (this.transform.parent.gameObject.activeSelf)
        {
            //myLine.Draw3DAuto();
            //myLine.active = true;
        }
        else
        {
            //myLine.active = false;
        }
	
	}

    void OnDisable() 
    {
        myLine.active = false;
    }

    void OnEnable()
    {
        if (myLine != null)
        {
            myLine.active = true;
        }
    }
}
