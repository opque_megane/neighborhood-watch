﻿using UnityEngine;
using System.Collections;

public class GlobalMaterials : MonoBehaviour {

    public static GlobalMaterials instance;
    public Material gridHouseLineMaterial;
    public Texture[] wallMaskTextures;
    public Material wallPlainMaterial;
    public Material frustumMaterial;
	
    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
