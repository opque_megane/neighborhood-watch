﻿using UnityEngine;
using System.Collections;

public class Toggler : MonoBehaviour {

    public bool onKeyPress;

    // Temporal
    float nextToggleTime = -1;
    public bool randomly;
    //periodically

    public MonoBehaviour toggle;

	// Use this for initialization
	void Start () {

        if (toggle == null)
        {
            toggle = (MonoBehaviour) (this.GetComponent<IToggle>());
        }

        updateToggleTime();

	
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time > nextToggleTime)
        {
            ((IToggle) toggle).toggle();
            updateToggleTime();
        }
	
	}

    void updateToggleTime()
    {
        nextToggleTime = Time.time + Random.Range(1, 8);
    }
}
