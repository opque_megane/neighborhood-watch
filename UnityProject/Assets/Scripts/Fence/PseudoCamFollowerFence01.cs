﻿using UnityEngine;
using System.Collections;

public class PseudoCamFollowerFence01 : MonoBehaviour {

    public Transform pseudoCamContainer;
    public Transform defaultView;
    Transform targetCameraTransform;

    public CameraPileCanvas[] overlays;

    int cameraIdx = 0;

	// Use this for initialization
	void Start () 
    {
        defaultView = new GameObject("Default View").transform;
        defaultView.transform.position = this.transform.position;
        defaultView.transform.rotation = this.transform.rotation;

        targetCameraTransform= defaultView;
	}
	
	// Update is called once per frame
	void LateUpdate () {
	
        if (Input.GetKeyDown(KeyCode.C))
        {
            goNextCameraView();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            goBirdsEyeCameraView();
        }




        this.transform.position = targetCameraTransform.position;
        this.transform.rotation = targetCameraTransform.rotation;
	}

    public void goNextCameraView()
    {
        pseudoCamContainer.GetChild(cameraIdx).GetComponent<CameraPileCamera>().setSelected(false);
        cameraIdx = (cameraIdx + 1) % pseudoCamContainer.childCount;
        targetCameraTransform = pseudoCamContainer.GetChild(cameraIdx).FindChild("viewPt");

        foreach(CameraPileCanvas canv in overlays)
        {
            canv.setScreenOverlay(cameraIdx +"");
        }
        pseudoCamContainer.GetChild(cameraIdx).GetComponent<CameraPileCamera>().setSelected(true);
    }

    public void goBirdsEyeCameraView()
    {
        targetCameraTransform= defaultView;
        foreach(CameraPileCanvas canv in overlays)
        {
            canv.setScreenOverlay("");
        }
    }

}
