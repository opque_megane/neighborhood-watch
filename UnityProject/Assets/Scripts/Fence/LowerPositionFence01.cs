﻿using UnityEngine;
using System.Collections;

public class LowerPositionFence01 : MonoBehaviour, IToggle {

    public KeyCode keyToLower = KeyCode.Alpha1;

    Vector3 startPos;
    float moveDist;
    float moveSpeed;

    bool targetIsStartPos = true;

    void Awake()
    {
        this.startPos = this.transform.localPosition;
    }
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(keyToLower))
        {
            targetIsStartPos = !targetIsStartPos;
        }    

        Vector3 target = targetIsStartPos ? startPos : startPos + Vector3.down * 3;

        this.transform.localPosition = Extensions.MovedTowardsTarget(this.transform.localPosition, target, Time.deltaTime * 4);
	}

    public void toggle()
    {
        targetIsStartPos = !targetIsStartPos;
    }
}
