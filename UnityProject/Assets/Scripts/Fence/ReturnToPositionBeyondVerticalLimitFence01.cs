﻿using UnityEngine;
using System.Collections;

public class ReturnToPositionBeyondVerticalLimitFence01 : MonoBehaviour {

    public Transform respawnPoint;
    public float verticalLimit = -15;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (this.transform.position.y < verticalLimit)
        {
            this.transform.position = respawnPoint.position;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
	    
	}
}
