﻿using UnityEngine;
using System.Collections;
using Vectrosity;

public class ManyCameraManagerFence01 : MonoBehaviour {


    public PseudoCamFollowerFence01 cameraFollower;
    public GameObject cameraPrefab;

    public GameObject screen1;
    public GameObject screen2;


	// Use this for initialization
	void Start () 
    {
        StartCoroutine(cameraSpawn());
	}
	
	// Update is called once per frame
	void LateUpdate () {

	}   

    void MakeAllCameras()
    {
       // GameObject cameraPrefab = Resources.Load<GameObject>("ManyCamera");

        int nToSpawn =  200;



        for (int i = 0; i < nToSpawn; i++)
        {
            GameObject newCamera = 
                (GameObject) GameObject.Instantiate(
                    cameraPrefab, 
                    this.transform.position + Random.insideUnitSphere * 5, 
                    Random.rotationUniform);

            cameraPrefab.GetComponent<ReturnToPositionBeyondVerticalLimitFence01>().respawnPoint = this.transform;

            //newCamera.transform.localScale = Vector3.Scale(newCamera.transform.localScale, (Vector3.one + Random.insideUnitSphere * 2.5f));

            newCamera.transform.localScale = Random.Range(.5f, 1.5f) * Vector3.one;

            newCamera.transform.parent = this.transform;
            //yield return new WaitForSeconds(.1f);

            newCamera.GetComponent<CameraPileCamera>().setSelected(false);

        }
    }
    IEnumerator cameraSpawn()
    {
        
        
        MakeAllCameras();
        for (;;)
        {

            //Reposition the monitors

            screen1.transform.position = Random.Range(-5, 5) * Vector3.forward + Random.Range(-5, 5) * Vector3.right;
            screen2.transform.position = Random.Range(-5, 5) * Vector3.forward + Random.Range(-5, 5) * Vector3.right;

            screen1.transform.eulerAngles = Random.Range(0, 360) * Vector3.up;
            screen2.transform.eulerAngles = Random.Range(0, 360) * Vector3.up;

            //yield return new WaitForSeconds(3); //Time to fall

            for (int i = 0; i < 10; i++)
            {
                yield return new WaitForSeconds(0.15f);
                //cameraFollower.goNextCameraView();
            }
            yield return new WaitForSeconds(10f);



            foreach (Transform t in this.transform)
            {

                t.transform.position = this.transform.position + Random.insideUnitSphere * 4;
                t.transform.rotation = Random.rotationUniform;
            }

        }

        Debug.Break();
        //yield return new WaitForSeconds(.1f);
    }
    void OnGUI()
    {
        Camera mainCam = Camera.main;
        for (int i = 0; i< this.transform.childCount; i++)
        {
            Vector3 screenPos = mainCam.WorldToScreenPoint(this.transform.GetChild(i).position);

            //if (i % 10 == 0)
           
                //AlexUtil.DrawText(new Vector2(screenPos.x, Screen.height - screenPos.y), "" + i, 24, Color.white, "");

        }
    }
}
