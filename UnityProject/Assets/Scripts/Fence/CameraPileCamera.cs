﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Vectrosity;

public class CameraPileCamera : MonoBehaviour {

    static int nCams  = 0;

    static Transform textCanvas;
    static GameObject textPrefab;
    Text text;
    int index = 0;
    public GameObject lens;
    int lensLayer = -1;

    VectorLine fovLine;



	// Use this for initialization
	
    void setupText()
    {
        /*GameObject textObj = GameObject.Instantiate(textPrefab);
        text = textObj.GetComponent<Text>();
        text.transform.parent = textCanvas;
        text.text = "" + index;*/
        foreach(Text txt in GetComponentsInChildren<Text>())
        {
            txt.text = "" + this.index;
        }

        lensLayer = lens.layer;

    }

    void Start () 
    {

        if (textPrefab == null)
        {
            textPrefab = Resources.Load<GameObject>("3DText");
            textCanvas = GameObject.Find("Canvas").transform;
        }

        index = nCams;
        nCams++;

    
        setupText();
      
       

       /* float fovDist = 25;
        float fovWidth = 8;
        float fovHeight = 7;

        fovLine = new VectorLine("vl", new List<Vector3>(){

            //outward lines
            Vector3.zero,
            new Vector3(fovWidth, -fovHeight, fovDist),

            Vector3.zero,
            new Vector3(-fovWidth, -fovHeight, fovDist),

            Vector3.zero,
            new Vector3(fovWidth, fovHeight, fovDist),

            Vector3.zero,
            new Vector3(-fovWidth, fovHeight, fovDist),

            //end rect
            new Vector3(-fovWidth, -fovHeight, fovDist),
            new Vector3(fovWidth, -fovHeight, fovDist),

            new Vector3(-fovWidth, fovHeight, fovDist),
            new Vector3(fovWidth, fovHeight, fovDist),

            new Vector3(-fovWidth, -fovHeight, fovDist),
            new Vector3(-fovWidth, fovHeight, fovDist),

            new Vector3(fovWidth, -fovHeight, fovDist),
            new Vector3(fovWidth, fovHeight, fovDist),

       

        }, 1);//VectorLine.SetRay(Color.black, Vector3.zero, Vector3.forward * 15);//this.transform.position, this.transform.forward * 4);

        fovLine.lineType = LineType.Discrete;
        fovLine.drawTransform = this.transform;
        fovLine.color = new Color(1,1,1, .5f);
        fovLine.Draw3DAuto(); */

	
	}

    public void setSelected(bool selected)
    {
        //this.GetComponentInChildren<MeshRenderer>().material.color = selected ? Color.yellow : Color.white;
        foreach (MeshRenderer mr in this.GetComponentsInChildren<MeshRenderer>())
        {
            foreach(Material m in mr.materials)
            {
                m.color = selected ? new Color(0,1,0) : Color.white;
            }
        }
        this.GetComponentInChildren<CameraFrustrumDrawer>().enabled = selected;
        lensLayer = selected ? LayerMask.NameToLayer("invisible_to_pov") : lensLayer;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (text != null)
        {
            Vector3 towardsCamera = (Camera.main.transform.position - this.transform.position).normalized;
            text.transform.position = this.transform.position;
            text.transform.position += .05f * towardsCamera;
        }

        //this.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
	}
}
