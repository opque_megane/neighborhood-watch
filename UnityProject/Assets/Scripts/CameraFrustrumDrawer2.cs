﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Vectrosity;

public class CameraFrustrumDrawer2 : MonoBehaviour {

   
    static int nCams  = 0;

    static Transform textCanvas;
    static GameObject textPrefab;
    Text text;
    int index = 0;

    VectorLine fovLine;

    float fovDist = 3;
    float fovWidth = 8;
    float fovHeight = 7;
    List<Transform> linePositions;

	// Use this for initialization
	
    void setupText()
    {
        GameObject textObj = GameObject.Instantiate(textPrefab);
        text = textObj.GetComponent<Text>();
        //text.transform.parent = textCanvas;
        //text.text = "" + index;

    }

    void Awake () {

      
        if (textPrefab == null)
        {
            //textPrefab = Resources.Load<GameObject>("3DText");
            //textCanvas = GameObject.Find("Canvas").transform;
        }

        index = nCams;
        nCams++;

    
        //setupText();
      

       

      

        fovLine = new VectorLine("vl", new List<Vector3>(){

            //outward lines
            Vector3.zero,
            new Vector3(fovWidth, -fovHeight, fovDist),

            Vector3.zero,
            new Vector3(-fovWidth, -fovHeight, fovDist),

            Vector3.zero,
            new Vector3(fovWidth, fovHeight, fovDist),

            Vector3.zero,
            new Vector3(-fovWidth, fovHeight, fovDist),

            //end rect
            new Vector3(-fovWidth, -fovHeight, fovDist),
            new Vector3(fovWidth, -fovHeight, fovDist),

            new Vector3(-fovWidth, fovHeight, fovDist),
            new Vector3(fovWidth, fovHeight, fovDist),

            new Vector3(-fovWidth, -fovHeight, fovDist),
            new Vector3(-fovWidth, fovHeight, fovDist),

            new Vector3(fovWidth, -fovHeight, fovDist),
            new Vector3(fovWidth, fovHeight, fovDist),

       

        }, 1);//VectorLine.SetRay(Color.black, Vector3.zero, Vector3.forward * 15);//this.transform.position, this.transform.forward * 4);

        fovLine.lineType = LineType.Discrete;
        fovLine.material = GlobalMaterials.instance.frustumMaterial;
        //fovLine.material.color = Color.white;
        fovLine.layer = LayerMask.NameToLayer("frustum");
        fovLine.drawTransform = this.transform;
        fovLine.color = new Color(1,1,1, 1);
        //fovLine.SetWidth(3);

        //fovLine.is2D = true;

        PostRenderLines r;
        fovLine.Draw3DAuto();

	    

        Vector3[] fovPts = new Vector3[] {            
            Vector3.zero,
            new Vector3(fovWidth, -fovHeight, fovDist),

            Vector3.zero,
            new Vector3(-fovWidth, -fovHeight, fovDist),

            Vector3.zero,
            new Vector3(fovWidth, fovHeight, fovDist),

            Vector3.zero,
            new Vector3(-fovWidth, fovHeight, fovDist),

            //end rect
            new Vector3(-fovWidth, -fovHeight, fovDist),
            new Vector3(fovWidth, -fovHeight, fovDist),

            new Vector3(-fovWidth, fovHeight, fovDist),
            new Vector3(fovWidth, fovHeight, fovDist),

            new Vector3(-fovWidth, -fovHeight, fovDist),
            new Vector3(-fovWidth, fovHeight, fovDist),

            new Vector3(fovWidth, -fovHeight, fovDist),
            new Vector3(fovWidth, fovHeight, fovDist),

            //----
            /*new Vector3(fovWidth, fovHeight, fovDist),
            new Vector3(fovWidth, -fovHeight, fovDist),

            new Vector3(-fovWidth, fovHeight, fovDist),
            new Vector3(-fovWidth, -fovHeight, fovDist),

            new Vector3(fovWidth, fovHeight, fovDist),
            new Vector3(-fovWidth, fovHeight, fovDist),

            new Vector3(fovWidth, -fovHeight, fovDist),
            new Vector3(-fovWidth, -fovHeight, fovDist),

            new Vector3(-fovWidth, fovHeight, fovDist),
            Vector3.zero,

            new Vector3(fovWidth, fovHeight, fovDist),
            Vector3.zero,

            new Vector3(-fovWidth, -fovHeight, fovDist),
            Vector3.zero,

            new Vector3(fovWidth, -fovHeight, fovDist),
            Vector3.zero,*/
            //-----
            };


        /*linePositions = new List<Transform>();
        foreach (Vector3 v in fovPts)
        {
            GameObject temp = new GameObject("fovPt");
            temp.transform.parent = this.transform;
            temp.transform.localPosition = v;
            temp.transform.localRotation = Quaternion.identity;
            linePositions.Add(temp.transform);
        }*/

	}
	
    void Update()
    {
        /*PostRenderFunctionPool.instance.addFunction(()=>{


            GL.PushMatrix();
            //lineMaterial.SetPass(0);
            //GL.LoadOrtho();//draw in screen space, w/o draws in world coordinates

            GL.Begin(GL.LINES);
            GL.Color(Color.white);
            //foreach(Transform t in linePositions)

             //   GL.Vertex(t.position);
            
            //GL.Vertex(Vector3.zero);
            //GL.Vertex(mousePos);//new Vector3(mousePos.x / Screen.width, mousePos.y / Screen.height, 0));
            GL.End();
            GL.PopMatrix();


        });*/
    }

	// Update is called once per frame
	void LateUpdate () {
        if (text != null)
        {
            Vector3 towardsCamera = (Camera.main.transform.position - this.transform.position).normalized;
            //text.transform.position = this.transform.position;
            //text.transform.position += .05f * towardsCamera;
        }

        PostRenderLines.instance.ptsToRender.Clear();

	}

    void OnPostRender()
    {
        //PostRenderFunctionPool.instance.doFuncs();
    }

    void OnDisable() 
    {
        fovLine.active = false;   
    }

    void OnEnable()
    {
        fovLine.active = true;
    }
        
}
