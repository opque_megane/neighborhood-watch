﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PostRenderFunctionPool : MonoBehaviour {

    public static PostRenderFunctionPool instance;
    public delegate void PostRenderFunc();
    List<PostRenderFunc> renderFuncs;


    void Awake()
    {
        instance = this;
        renderFuncs = new List<PostRenderFunc>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        renderFuncs.Clear();
	}

    public void addFunction(PostRenderFunc func)
    {
        this.renderFuncs.Add(func);
    }

    public void doFuncs()
    {
        foreach(PostRenderFunc func in this.renderFuncs)
        {
            func();
        }
    }
}
