﻿using UnityEngine;
using System.Collections;

public class PseudoCamFollower : MonoBehaviour {

    public Transform pseudoCamContainer;

    int cameraIdx = 0;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
	
        if (Input.GetKeyDown(KeyCode.C))
        {
            cameraIdx = (cameraIdx + 1) % pseudoCamContainer.childCount;
        }

        Transform targetCameraTransform = pseudoCamContainer.GetChild(cameraIdx);

        this.transform.position = targetCameraTransform.position;
        this.transform.rotation = targetCameraTransform.rotation;
	}
}
