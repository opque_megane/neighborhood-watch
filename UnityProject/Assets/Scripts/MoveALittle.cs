﻿using UnityEngine;
using System.Collections;

public class MoveALittle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        float t = Time.time;
        float s = 6 * Mathf.Sin(t);
        float c = 6 * Mathf.Cos(t);
        //float c = Mathf.Sin(t);
        this.transform.eulerAngles = new Vector3(s, 0, c);
	}
}
