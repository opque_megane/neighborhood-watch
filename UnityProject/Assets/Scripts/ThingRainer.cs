﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using UnityEngine.UI;

public class ThingRainer : MonoBehaviour {

    public GameObject cameraPrefab;
    float spawnRadius = 10;


	// Use this for initialization
	void Start () 
    {
        StartCoroutine(cameraSpawn());
	}
	
	// Update is called once per frame
	void LateUpdate () {

	}   
    IEnumerator cameraSpawn()
    {
        //GameObject cameraPrefab = Resources.Load<GameObject>("ManyCamera");

        int nToSpawn =  50;




        for (int i = 0; i < nToSpawn; i++)
        {
            GameObject newCamera = 
                (GameObject) GameObject.Instantiate(
                    cameraPrefab, 
                    this.transform.position + Vector3.Scale(new Vector3(1,0,1) , Random.insideUnitSphere * spawnRadius), 
                    Random.rotationUniform);
                    
                    cameraPrefab.GetComponent<ReturnToPositionBeyondVerticalLimit>().respawnPoint = this.transform;

            //newCamera.transform.localScale = Vector3.Scale(newCamera.transform.localScale, (Vector3.one + Random.insideUnitSphere * 2.5f));

            newCamera.transform.parent = this.transform;
            yield return new WaitForSeconds(1.5f);

            if (MonitorFence.instance != null)
            {
                newCamera.name = Random.Range(0, 200) + newCamera.name;
                newCamera.AddComponent<Camera>();
                newCamera.GetComponent<Camera>().targetTexture = MonitorFence.instance.getMonitor(i).rt;//MonitorTube.instance.getMonitor(i).rt;
                newCamera.GetComponent<Camera>().nearClipPlane = .01f;






            }
        }
    }
    void OnGUI()
    {
        Camera mainCam = Camera.main;
        for (int i = 0; i< this.transform.childCount; i++)
        {
            Vector3 screenPos = mainCam.WorldToScreenPoint(this.transform.GetChild(i).position);

            //if (i % 10 == 0)
           
                AlexUtil.DrawText(new Vector2(screenPos.x, Screen.height - screenPos.y), "" + i, 24, Color.white, "");

        }
    }
}
