﻿using UnityEngine;
using System.Collections;

public class MoveSimple : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal2"),0,Input.GetAxis("Vertical2"));
        move = this.transform.TransformDirection(move);
        move.y = 0;

        move += Vector3.up * Input.GetAxis("UpDown");
        move.Normalize();

        this.transform.position += move * 5 * Time.deltaTime;


        Vector3 eulerRotation = new Vector3(-Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), 0);
        this.transform.eulerAngles += eulerRotation * Time.deltaTime * 17;
        Vector3 noZEuler = this.transform.eulerAngles;
        noZEuler.z = 0;
        this.transform.eulerAngles = noZEuler;
	}

    void LateUpate()
    {
     
    }
}
