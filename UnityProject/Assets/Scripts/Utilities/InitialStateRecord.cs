﻿/*

Script that records some initial properties 
of a game object and its components.  Mostly,
just its transform.

You can save any value by calling save<T>()
and then retrieve that value by calling
getSaved<T>()

Mostly only used for position, rotation, scale
and not that widely used.

save initial position, size, material properties
is something that is done all the time when animating
via code, and with this script I hoped to reduce
the number of these startPosition, startScale
variables.


*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class InitialStateRecord : MonoBehaviour
{


    //By default, save some of the common
    //transform properties
     Quaternion rotation;
     Quaternion localRotation;
     Vector3 localScale;
     Vector3 position;
     Vector3 localPosition;
     public Dictionary<string, object> savedProperties;

    //startPosition is extra common, so I made a cleaner way of requesting it
    public Vector3 startPosition
    {
        get
        {
            return getSaved<Vector3>("Transform.position");
        }
    }

    //the default properties to save
    //It would be smart to publicaly expose some functions
    //to save common subsets of properties (e.g. all material properties)
    //all common transform properties
    readonly string[] defaultSaveProperties = { "Transform.position", "Transform.localPosition", "Transform.eulerAngles", "Transform.localScale" };

    public void Start()
    {
    }

    //This should be moved into the Awake() function
    //Unity components don't have constructors per se,
    //but Awake functions like one.
    //This function does the necessary set up for
    //the component, and currently you have to call
    //it yourself before you can use it.
    public void Record()
    {
        Transform t = this.transform;

        savedProperties = new Dictionary<string, object>();
        rotation = t.rotation;
        localScale = t.localScale;
        position = t.position;


        foreach (string propString in defaultSaveProperties)
        {

            save<Vector3>(propString);
            //Debug.Log(thingOut);
        }

    

        //reflection used to get property by a string name, 
        //A more nuclear option for saving a bunch of stuff without having to make a ton of variables.
        //typeof(Transform).GetProperty("position").GetValue(t, null);

        //In the start function, you'd specify what values you wanted to save, via an array of strings, perhaps with '.' notation
        //for getting values from other components(?)
        //SpriteRenderer.color
        //Transform.position
        //Tranform.eulerAngles

    }

    

    /*
    
    To save value, you provide a string formatted
    '<component type name><.><property name>'

    e.g.

    Transform.eulerAngles

    gameObject.Layer (this one is a special case)

    RigidBody.mass

    */
    public void save<T>(string propString)
    {
        string[] props = propString.Split('.');
        string componentName = props[0];
        string propName = props[1];

        object thingOut = null;

        if (componentName == "gameObject")
        {
             thingOut = gameObject.GetType().GetProperty(propName).GetValue(gameObject, null);
        }
        else
        {
            Component gottenComponent = this.GetComponent(componentName);
             thingOut = gottenComponent.GetType().GetProperty(propName).GetValue(gottenComponent, null);
        }


        savedProperties[propString] = thingOut;
    }


    /*

    use the same kind of string format described for save<T>()
    to retrieve a value
    */

    public T getSaved<T>(string propName)
    {
        return (T) savedProperties[propName];
    }

}
