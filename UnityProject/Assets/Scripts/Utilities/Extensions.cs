﻿/*

A combination of extensions methods (more info below)
https://msdn.microsoft.com/en-us/library/bb383977.aspx

and general utility methods to simplify some common opperations

including ...
- converting between Vector2 and Vector3
- Linearly moving a Vector3 towards another Vector3
- Linearly moving a number towards another number
- Calling a function on delay
- A pogramatic animation template function (varyWithT())

*/

using UnityEngine;
using System.Collections;

public static class Extensions  {
    
    public static Vector2 AsXyVector2(this Vector3 thiss)
    {
        return new Vector2(thiss.x, thiss.y);
    }

    public static Vector3 NoZ(this Vector3 thiss)
    {
        return new Vector3(thiss.x, thiss.y, 0);
    }

     public static Vector3 AsVector3NoZ(this Vector2 thiss)
    {
        return new Vector3(thiss.x, thiss.y, 0);
    }

     public static Vector3 AsVector3NoY(this Vector2 thiss)
     {
         return new Vector3(thiss.x, 0, thiss.y);
     }

     //Return a new Vector3 in which 'currentPosition'
     //has moved closer to 'target' by 'speed' amount.
     //Won't overshoot the target.

     //It's not terribly complicated, but it's such a common operation
     //that it's nice to have as its own function.
    public static Vector3 MovedTowardsTarget(Vector3 currentPosition, Vector3 target, float maxSpeed)
    {

        Vector3 towardsTarg = (target - currentPosition);//.NoZ();

        float coeff = Mathf.Clamp01(1 - towardsTarg.magnitude / 5.0f);


        float effMag = Mathf.Min(maxSpeed, towardsTarg.magnitude);

        return currentPosition + towardsTarg.normalized * effMag;
    }


     //Return a new float in which 'current'
     //has moved closer to 'target' by 'maxSpeed' amount.
     //Won't overshoot 'target'.

     //It's not terribly complicated, but it's such a common operation
     //that it's nice to have as its own function.
    public static float MovedTowards(float current, float target, float maxSpeed)
    {

        float ret = -9999;

        if (current < target)
        {
            ret = current + maxSpeed;
            ret = Mathf.Min(ret, target); //don't overshoot target!!
        }
        else// if (current > target)
        {
            ret = current - maxSpeed;
            ret = Mathf.Max(ret, target);//don't overshoot target!!
        }

        return ret;

        

        //don't overshoot!
    }



    public delegate void NoArgNoRetFunction();

    public delegate void ArgIsTransform(Transform t);
    
    public delegate void TFunction(float t);


    //Calls a function 'tfunc' repeatedly over a duration of 'dur' seconds
    //  the parameter 't' of 'tfunc' is the normalized time.
    //i.e. the first time, it will call tfunc(0.0f), halfway through 'dur' seconds it will
    //call tfunc(0.5f), and after 'dur' seconds have elapsed it will call
    //tfunc(1.0f).  This is a really convenient way to do programatic animations,
    //and you will see it used extensively in the scripts.
    //What's actually happening isn't too complicated, but it's a little clunky
    //to write from scratch every time.  You can see explicitly what's happening in
    // genericT() further down
    public static void varyWithT(this MonoBehaviour thiss, TFunction tfunc, float dur)
    {
        thiss.StartCoroutine(genericT(tfunc, dur));
    }



    /*
    This function will call 'func()' on the transform, and all its descendants.
    */
    public static void doToAllChildren(this Transform thiss, ArgIsTransform func)
    {
        func(thiss);
        foreach (Transform child in thiss)
        {
            child.doToAllChildren(func);
        }
    }

    /*
        This calls the anonymous function 'func' after 'delay' seconds have passed

    */
    public static void delayedFunction(this MonoBehaviour thiss, NoArgNoRetFunction func, float delay)
    {
        thiss.StartCoroutine(callAfter(func, delay));
    }

    //will get the starting local position of an object
    //if it was set up with an InitialStateRecord
    //(not widely used)
    public static Vector3 startLocalPosition(this Transform thiss)
    {
        return thiss.GetComponent<InitialStateRecord>().getSaved<Vector3>("Transform.localPosition");
    }

    public static Vector3 startPosition(this Transform thiss)
    {
        return thiss.GetComponent<InitialStateRecord>().getSaved<Vector3>("Transform.position");
    }


    static IEnumerator callAfter(NoArgNoRetFunction func, float delay)
    {
        yield return new WaitForSeconds(delay);
        func();
    }

    public static IEnumerator genericT(TFunction tfunc, float dur)
    {
        float startTime = Time.time;

        while (Time.time <= startTime + dur)
        {
            float t = Mathf.Clamp01((Time.time - startTime) / dur);
            tfunc(t);
            yield return new WaitForEndOfFrame();
        }
        //force call with 1
        tfunc(1);
    }



    public static Vector2 asVector2(this Direction thiss)
    {
        switch(thiss)
        {
            case Direction.LEFT:
                return Vector2.left;
            case Direction.RIGHT:
                return Vector2.right;
                
            case Direction.UP:
                return Vector2.up;
                
            default://case Direction.DOWN:
                return Vector2.down;
                
        }
    }



    public static readonly Direction[] AllDirections = { Direction.LEFT, Direction.RIGHT, Direction.UP, Direction.DOWN };


    //
    public static Direction NearestDirection(Vector2 v)
    {
        Vector2 nomd = v.normalized;
        Direction ret = Direction.UP;
        float maxDot = float.NegativeInfinity;
        foreach (Direction d in AllDirections)
        {
            float dot = Vector2.Dot(d.asVector2(), v);
            if (dot > maxDot)
            {
                ret = d;
                maxDot = dot;
            }
        }
        return ret;
    }

}

public enum Direction
{
    LEFT = 0, RIGHT = 1, UP = 2, DOWN = 3
}
