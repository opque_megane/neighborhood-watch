﻿using UnityEngine;
using System.Collections;

public interface IToggle  {

    GameObject gameObject
    {
        get;
    }

    void toggle();


}
