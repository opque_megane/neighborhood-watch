﻿using UnityEngine;
using System.Collections;

public class PanoramaCam : MonoBehaviour {

    static int camerasSoFar = 0;
    int index = -1;
    public static int nCameras = -1;


	// Use this for initialization
	void Start () 
    {
        index = camerasSoFar;
        camerasSoFar++;


        StartCoroutine(occasionalCam());
	}

    IEnumerator occasionalCam()
    {
        for(;;)
        {
            foreach (Camera c in GetComponentsInChildren<Camera>())
            {
                c.enabled = true;
            }
            yield return new WaitForEndOfFrame();

            foreach (Camera c in GetComponentsInChildren<Camera>())
            {
                c.enabled = false;
            }
            yield return new WaitForEndOfFrame();
            //yield return new WaitForSeconds(3 * Random.Range(.29f, .31f));
        }
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.eulerAngles += Vector3.up * Time.deltaTime * 10;
	
	}

    public void setupCameras(RenderTexture rt)
    {
        
    }
}
