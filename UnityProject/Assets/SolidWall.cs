﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SolidWall : MonoBehaviour {

    //public Texture maskTexture;
    public Material maskedMaterial;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.J))
        {
            shuffle();
        }
	}


    void reduceToRandomSubset(IList l, int subsetSize)
    {
        //int nToRemove = l.Count - subsetSize;
        while (l.Count > subsetSize)
        {
            l.RemoveAt(Random.Range(0, l.Count));
        }
    }

    public static void Shuffle(IList list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            int k = Random.Range(0, n);
            n--;  
            var value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }

    public void shuffle()
    {

        bool coinFlip = Random.Range(0, 100) >= 7;
        string backOrFront = coinFlip ? "back" : "front";
        string backOrFrontInv = !coinFlip ? "back" : "front";




        // --- mask --------

        List<Texture> possibleTexs = new List<Texture>{WebcamManager.instance.getWebcamTexture(0), WebcamManager.instance.getWebcamTexture(1)};
        reduceToRandomSubset(possibleTexs,2);
        Shuffle(possibleTexs);
        //possibleTexs

        this.transform.FindChild(backOrFront).GetComponent<Renderer>().material = maskedMaterial;

        this.transform.FindChild(backOrFrontInv).GetComponent<Renderer>().material = GlobalMaterials.instance.wallPlainMaterial;//WebcamManager.instance.getWebcamTexture(Random.Range(0,2));


        Texture tex1 = possibleTexs[0];
        Texture tex2 = possibleTexs[1];

        this.transform.FindChild(backOrFront).GetComponent<Renderer>().material.SetTexture("_MainTex", tex1);
        this.transform.FindChild(backOrFront).GetComponent<Renderer>().material.SetTexture("_Tex2", tex2);
        this.transform.FindChild(backOrFront).GetComponent<Renderer>().material.SetTexture("_AlphaTex", GlobalMaterials.instance.wallMaskTextures[Random.Range(0, GlobalMaterials.instance.wallMaskTextures.Length)]);




        // ---- plain -------
        if (false)
        {
            //this.transform.FindChild("back").GetComponent<Renderer>().material = plainMaterial;
        }
    }
}
