﻿using UnityEngine;
using System.Collections;

public class GridBuildingWall : MonoBehaviour {

    public Transform perforatedWall;
    public Transform solidWall;
    public Transform lines;
    public enum WallType
    {
        Solid, Perforated, Empty
    };

    WallType wallType = WallType.Solid;

	// Use this for initialization
	void Start () {
        //WallType[] allWallTypes = {WallType.Solid, WallType.Perforated, WallType.Empty};
        //setWallType(allWallTypes[Random.Range(0, (int) allWallTypes.Length)]);

        Transform[] wallContainers = new Transform[] {perforatedWall, solidWall};
        foreach(Transform wall in wallContainers)
        {
            print(wall.name + " - "  + wall.FindChild("back").GetComponent<Renderer>());
            //List<Texture> possibleTexs = new List<Texture>{WebcamManager.instance.getWebcamTexture(0), WebcamManager.instance.getWebcamTexture(1)};
            //wall.FindChild("back").GetComponent<Renderer>().material.mainTexture = WebcamManager.instance.getWebcamTexture(Random.Range(0,2));
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setWallType(WallType newType)
    {
        perforatedWall.gameObject.SetActive(false);
        solidWall.gameObject.SetActive(false);
        wallType = newType;

        if (newType == WallType.Solid)
        {
            solidWall.gameObject.SetActive(true);
            solidWall.GetComponent<SolidWall>().shuffle();
        }
        else  if (newType == WallType.Perforated)
        {
            bool coinFlip = Random.Range(0, 100) >= 7;
            string backOrFront = coinFlip ? "back" : "front";
            string backOrFrontInv = !coinFlip ? "back" : "front";
            perforatedWall.gameObject.SetActive(true);
            perforatedWall.FindChild(backOrFront).GetComponent<Renderer>().material.mainTexture = WebcamManager.instance.getWebcamTexture(Random.Range(0,2));
            perforatedWall.FindChild(backOrFrontInv).GetComponent<Renderer>().material.mainTexture = null;//WebcamManager.instance.getWebcamTexture(Random.Range(0,2));
        } 
        else // WallType.Empty
        {
            
        }
        
    }
}
