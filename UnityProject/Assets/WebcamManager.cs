﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WebcamManager : MonoBehaviour {

    public static WebcamManager instance;
    public Texture fakeCamera2Texture;

    List<Texture> camTextures;
    //public string[] excludeCamera;

    void Awake() 
    {
        instance = this;
        print("FIIIIIIIIIIIIIRst");
        camTextures = new List<Texture>();
        WebCamDevice[] devices = UnityEngine.WebCamTexture.devices;

        for( int i = 0 ; i < devices.Length ; i++ )
        {
            Debug.Log(devices[i].name);  

            UnityEngine.WebCamTexture webcamTexture = new  UnityEngine.WebCamTexture(devices[i].name);
            //webcamTexture.wrapMode = TextureWrapMode.Repeat;
            webcamTexture.Play();

            camTextures.Add(webcamTexture);

        }



        if (fakeCamera2Texture != null)
        {
            camTextures.Add(fakeCamera2Texture);
        }
 
    }
	
	// Update is called once per frame
	void LateUpdate () {

	
	}

    public Texture getWebcamTexture(int i)
    {
        return camTextures[i];
    }
}
