﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class CamGridMaker : MonoBehaviour {

    public float width = 40;
    public int rows = 2;
    public GameObject panCamPrefab;
    VectorLine lineGrid;

    /*public GameObject screen;
    public RenderTextureScreen ezScreen;*/

    RenderTexture renderTexture;




	// Use this for initialization
	void Start () {
        float height = width;
        int columns = rows;

        renderTexture = new RenderTexture(1024 , 2048, 16);
        renderTexture.Create();

        int nCams = columns * rows * 4;//4 camers in each panorama;

        //int nRowsScreenCells = (int)Mathf.Floor(Mathf.Sqrt(nCams));
        //int nColsScreenCells = (int)Mathf.Ceil(nCams /nRowsScreenCells);



        //screen.GetComponent<Renderer>().material.mainTexture = renderTexture;

        //ezScreen.setup(columns * rows, 4);
        List<Vector3> gridPts = new List<Vector3>();

        int cameraI = 0;
        for (int i = 0; i <= rows; i++)
        {
            for (int j = 0; j <= columns; j++)
            {
                float r = ((0.0f + i) / rows);
                float c = ((0.0f + j) / columns);

                r -= .5f;
                c -= .5f;
                Vector3 spawnPos = r * Vector3.forward * height
                    + c * Vector3.right * width;

                //Make grid lines...
                if (i == j)
                {
                    //horizontal(?) lines
                    gridPts.Add(new Vector3(-.5f * width, 0, 0) + new Vector3(0,0, c * height));
                    gridPts.Add(new Vector3(.5f * width, 0, 0) + new Vector3(0,0, c * height));

                    //vertical(?) lines
                    gridPts.Add(new Vector3( 0, 0,-.5f * height) + new Vector3( r * width,0,0));
                    gridPts.Add(new Vector3(0,0,.5f * height) + new Vector3(r * width,0,0));
                }

         

                GameObject panoCam = (GameObject) GameObject.Instantiate(panCamPrefab, Vector3.zero, Quaternion.identity);
                panoCam.transform.parent = this.transform;
                panoCam.transform.localPosition = spawnPos;





            }
        }
       
	
        lineGrid = new VectorLine("gridd", gridPts, 1);
        lineGrid.drawTransform = this.transform;
        lineGrid.material = GlobalMaterials.instance.gridHouseLineMaterial;
        lineGrid.Draw3DAuto();


  

        shuffle();
	}

    void shuffle()
    {
        this.transform.position = Vector3.up * Random.Range(2, 12) + Vector3.right * Random.Range(-8, 8) + Vector3.forward * Random.Range(-8, 8);
        Vector3 randomUp = Random.onUnitSphere;

        randomUp.x *= .5f;
        randomUp.y = Mathf.Abs(randomUp.y);
        randomUp.Normalize();
        this.transform.up = randomUp;//;;Random.rotationUniform;
        this.transform.Rotate(Vector3.up *  Random.Range(0, 360),Space.Self);

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            shuffle();
        }
	}


}
