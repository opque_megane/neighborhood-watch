﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObeyCameraZoneRules : MonoBehaviour {

    bool previousZoneStatus = false;
    bool justEnteredValidZone = false;
    bool inValidZone = false;
    static Material mat;
    float flashTime = float.NegativeInfinity;
    float leftZoneTime = float.NegativeInfinity;
	// Use this for initialization

    GameObject monitorText;
    static int camCount = 0;
    int camIdx;

    Color flashColor = Color.yellow;

    int defaultCullingMask;// = this.GetComponent<Camera>().cullingMask;
    void Awake()
    {
        camCount++;
        camIdx = camCount;


        if (monitorText == null)
        {
            monitorText = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/WorldLabel"));
            monitorText.GetComponentInChildren<Text>().color = Color.white;
            text = "X";
        }
    }



    void Start () {



        defaultCullingMask = this.GetComponent<Camera>().cullingMask;


        if (mat == null)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things. In this case, we just want to use
            // a blend mode that inverts destination colors.            
            var shader = Shader.Find ("Hidden/Internal-Colored");
            mat = new Material (shader);
            mat.hideFlags = HideFlags.HideAndDontSave;
            // Set blend mode to invert destination colors.
            mat.SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            mat.SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn off backface culling, depth writes, depth test.
            mat.SetInt ("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            mat.SetInt ("_ZWrite", 0);
            mat.SetInt ("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
        }

	}

    public void positionMonitorText(Vector3 position, Quaternion rotation, Vector3 size)
    {
        if (monitorText == null)
        {
            monitorText = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/WorldLabel"));
            monitorText.GetComponentInChildren<Text>().color = Color.white;
            text = "X";
        }
        monitorText.transform.position = position;
        monitorText.transform.rotation = rotation;
        monitorText.transform.localScale = Mathf.Min(size.x, size.y) * Vector3.one;
    }

    string text
    {
        get { return monitorText.GetComponentInChildren<Text>().text;}
        set { monitorText.GetComponentInChildren<Text>().text = value;}
    }
	
	// Update is called once per frame
	void Update () {



        text = "";

        this.GetComponent<Camera>().cullingMask = defaultCullingMask;
        this.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
        text = ""+camIdx;

        if (!inValidZone)
        {
            
            if (Time.time > leftZoneTime + .5f) //don't immediately turn off
            {
                this.GetComponent<Camera>().cullingMask = 0;
                this.GetComponent<Camera>().clearFlags = CameraClearFlags.Color;
                this.GetComponent<Camera>().backgroundColor = Color.black;
                text = "";
            }

        }


        if (previousZoneStatus != inValidZone)
        {
            flashColor = Color.yellow;
            flashTime = Time.time;
            if (!inValidZone)
            {
                flashColor = Color.black;
                leftZoneTime = Time.time;
            }
            else
            {
                //TODO : this will corrupt the original color if called in the middle
       
            }
        }



        previousZoneStatus = inValidZone;
        inValidZone = false;
        justEnteredValidZone = false;
	}

    void OnTriggerStay(Collider other)
    {
        inValidZone = true;
    }

    void OnTriggerEnter(Collider other)
    {
        justEnteredValidZone = true;
        Color startColor = this.GetComponentInChildren<Renderer>().material.color;
        this.varyWithT((float t) => {


            this.GetComponentInChildren<Renderer>().material.color = Color.Lerp(Color.yellow, Color.black, t);
        }, .25f);

    }



    // ================================






    void OnPostRender() {





        // Black out screen
        float t = Mathf.Clamp01((Time.time - flashTime) / 0.75f);

        if (Mathf.Abs(t) > .01)
        {

            GL.PushMatrix();
            mat.SetPass(0);
            GL.LoadOrtho();//LoadPixelMatrix();
            GL.Begin(GL.QUADS);
            Color adjustedColor = flashColor;
            if (flashColor == Color.yellow)
            {
                adjustedColor.a = (1-t);
            }
            else
            {
                adjustedColor.a = t;
            }
            GL.Color(adjustedColor);//new Color(1,1,0,(1-t)));
            GL.Vertex3(0, 0, 0);
            GL.Vertex3(1, 0, 0);
            GL.Vertex3(1, 1, 0);
            GL.Vertex3(0, 1, 0);

            GL.End();
            GL.PopMatrix();
        }
    }
}
