﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class MultiCamLineTest : MonoBehaviour {

    List<Vector3> pts = new List<Vector3>() {new Vector3(-0.476f, -1f, -0.155f), new Vector3(-0.476f, 1f, -0.155f), new Vector3(-0.476f, 1f, -0.155f), new Vector3(-0.405f, 1f, -0.294f), new Vector3(-0.405f, 1f, -0.294f), new Vector3(-0.476f, -1f, -0.155f), new Vector3(-0.405f, 1f, -0.294f), new Vector3(-0.405f, -1f, -0.294f), new Vector3(-0.405f, -1f, -0.294f), new Vector3(-0.476f, -1f, -0.155f), new Vector3(-0.405f, 1f, -0.294f), new Vector3(-0.294f, 1f, -0.405f), new Vector3(-0.294f, 1f, -0.405f), new Vector3(-0.405f, -1f, -0.294f), new Vector3(-0.294f, 1f, -0.405f), new Vector3(-0.294f, -1f, -0.405f), new Vector3(-0.294f, -1f, -0.405f), new Vector3(-0.405f, -1f, -0.294f), new Vector3(-0.294f, 1f, -0.405f), new Vector3(-0.155f, 1f, -0.476f), new Vector3(-0.155f, 1f, -0.476f), new Vector3(-0.294f, -1f, -0.405f), new Vector3(-0.155f, 1f, -0.476f), new Vector3(-0.155f, -1f, -0.476f), new Vector3(-0.155f, -1f, -0.476f), new Vector3(-0.294f, -1f, -0.405f), new Vector3(-0.155f, 1f, -0.476f), new Vector3(0f, 1f, -0.5f), new Vector3(0f, 1f, -0.5f), new Vector3(-0.155f, -1f, -0.476f), new Vector3(0f, 1f, -0.5f), new Vector3(0f, -1f, -0.5f), new Vector3(0f, -1f, -0.5f), new Vector3(-0.155f, -1f, -0.476f), new Vector3(0f, 1f, -0.5f), new Vector3(0.155f, 1f, -0.476f), new Vector3(0.155f, 1f, -0.476f), new Vector3(0f, -1f, -0.5f), new Vector3(0.155f, 1f, -0.476f), new Vector3(0.155f, -1f, -0.476f), new Vector3(0.155f, -1f, -0.476f), new Vector3(0f, -1f, -0.5f), new Vector3(0.155f, 1f, -0.476f), new Vector3(0.294f, 1f, -0.405f), new Vector3(0.294f, 1f, -0.405f), new Vector3(0.155f, -1f, -0.476f), new Vector3(0.294f, 1f, -0.405f), new Vector3(0.294f, -1f, -0.405f), new Vector3(0.294f, -1f, -0.405f), new Vector3(0.155f, -1f, -0.476f), new Vector3(0.294f, 1f, -0.405f), new Vector3(0.405f, 1f, -0.294f), new Vector3(0.405f, 1f, -0.294f), new Vector3(0.294f, -1f, -0.405f), new Vector3(0.405f, 1f, -0.294f), new Vector3(0.405f, -1f, -0.294f), new Vector3(0.405f, -1f, -0.294f), new Vector3(0.294f, -1f, -0.405f), new Vector3(0.405f, 1f, -0.294f), new Vector3(0.476f, 1f, -0.155f), new Vector3(0.476f, 1f, -0.155f), new Vector3(0.405f, -1f, -0.294f), new Vector3(0.476f, 1f, -0.155f), new Vector3(0.476f, -1f, -0.155f), new Vector3(0.476f, -1f, -0.155f), new Vector3(0.405f, -1f, -0.294f), new Vector3(0.476f, 1f, -0.155f), new Vector3(0.5f, 1f, 0f), new Vector3(0.5f, 1f, 0f), new Vector3(0.476f, -1f, -0.155f), new Vector3(0.5f, 1f, 0f), new Vector3(0.5f, -1f, 0f), new Vector3(0.5f, -1f, 0f), new Vector3(0.476f, -1f, -0.155f), new Vector3(0.5f, 1f, 0f), new Vector3(0.476f, 1f, 0.155f), new Vector3(0.476f, 1f, 0.155f), new Vector3(0.5f, -1f, 0f), new Vector3(0.476f, 1f, 0.155f), new Vector3(0.476f, -1f, 0.155f), new Vector3(0.476f, -1f, 0.155f), new Vector3(0.5f, -1f, 0f), new Vector3(0.476f, 1f, 0.155f), new Vector3(0.405f, 1f, 0.294f), new Vector3(0.405f, 1f, 0.294f), new Vector3(0.476f, -1f, 0.155f), new Vector3(0.405f, 1f, 0.294f), new Vector3(0.405f, -1f, 0.294f), new Vector3(0.405f, -1f, 0.294f), new Vector3(0.476f, -1f, 0.155f), new Vector3(0.405f, 1f, 0.294f), new Vector3(0.294f, 1f, 0.405f), new Vector3(0.294f, 1f, 0.405f), new Vector3(0.405f, -1f, 0.294f), new Vector3(0.294f, 1f, 0.405f), new Vector3(0.294f, -1f, 0.405f), new Vector3(0.294f, -1f, 0.405f), new Vector3(0.405f, -1f, 0.294f), new Vector3(0.294f, 1f, 0.405f), new Vector3(0.155f, 1f, 0.476f), new Vector3(0.155f, 1f, 0.476f), new Vector3(0.294f, -1f, 0.405f), new Vector3(0.155f, 1f, 0.476f), new Vector3(0.155f, -1f, 0.476f), new Vector3(0.155f, -1f, 0.476f), new Vector3(0.294f, -1f, 0.405f), new Vector3(0.155f, 1f, 0.476f), new Vector3(0f, 1f, 0.5f), new Vector3(0f, 1f, 0.5f), new Vector3(0.155f, -1f, 0.476f), new Vector3(0f, 1f, 0.5f), new Vector3(0f, -1f, 0.5f), new Vector3(0f, -1f, 0.5f), new Vector3(0.155f, -1f, 0.476f), new Vector3(0f, 1f, 0.5f), new Vector3(-0.155f, 1f, 0.476f), new Vector3(-0.155f, 1f, 0.476f), new Vector3(0f, -1f, 0.5f), new Vector3(-0.155f, 1f, 0.476f), new Vector3(-0.155f, -1f, 0.476f), new Vector3(-0.155f, -1f, 0.476f), new Vector3(0f, -1f, 0.5f), new Vector3(-0.155f, 1f, 0.476f), new Vector3(-0.294f, 1f, 0.405f), new Vector3(-0.294f, 1f, 0.405f), new Vector3(-0.155f, -1f, 0.476f), new Vector3(-0.294f, 1f, 0.405f), new Vector3(-0.294f, -1f, 0.405f), new Vector3(-0.294f, -1f, 0.405f), new Vector3(-0.155f, -1f, 0.476f), new Vector3(-0.294f, 1f, 0.405f), new Vector3(-0.405f, 1f, 0.294f), new Vector3(-0.405f, 1f, 0.294f), new Vector3(-0.294f, -1f, 0.405f), new Vector3(-0.405f, 1f, 0.294f), new Vector3(-0.405f, -1f, 0.294f), new Vector3(-0.405f, -1f, 0.294f), new Vector3(-0.294f, -1f, 0.405f), new Vector3(-0.405f, 1f, 0.294f), new Vector3(-0.476f, 1f, 0.155f), new Vector3(-0.476f, 1f, 0.155f), new Vector3(-0.405f, -1f, 0.294f), new Vector3(-0.476f, 1f, 0.155f), new Vector3(-0.476f, -1f, 0.155f), new Vector3(-0.476f, -1f, 0.155f), new Vector3(-0.405f, -1f, 0.294f), new Vector3(-0.476f, 1f, 0.155f), new Vector3(-0.5f, 1f, 0f), new Vector3(-0.5f, 1f, 0f), new Vector3(-0.476f, -1f, 0.155f), new Vector3(-0.5f, 1f, 0f), new Vector3(-0.5f, -1f, 0f), new Vector3(-0.5f, -1f, 0f), new Vector3(-0.476f, -1f, 0.155f), new Vector3(-0.5f, 1f, 0f), new Vector3(-0.476f, 1f, -0.155f), new Vector3(-0.476f, 1f, -0.155f), new Vector3(-0.5f, -1f, 0f), new Vector3(-0.476f, -1f, -0.155f), new Vector3(-0.5f, -1f, 0f), new Vector3(-0.405f, -1f, -0.294f), new Vector3(0f, -1f, 0f), new Vector3(0f, -1f, 0f), new Vector3(-0.476f, -1f, -0.155f), new Vector3(-0.294f, -1f, -0.405f), new Vector3(0f, -1f, 0f), new Vector3(-0.155f, -1f, -0.476f), new Vector3(0f, -1f, 0f), new Vector3(0f, -1f, -0.5f), new Vector3(0f, -1f, 0f), new Vector3(0.155f, -1f, -0.476f), new Vector3(0f, -1f, 0f), new Vector3(0.294f, -1f, -0.405f), new Vector3(0f, -1f, 0f), new Vector3(0.405f, -1f, -0.294f), new Vector3(0f, -1f, 0f), new Vector3(0.476f, -1f, -0.155f), new Vector3(0f, -1f, 0f), new Vector3(0.5f, -1f, 0f), new Vector3(0f, -1f, 0f), new Vector3(0.476f, -1f, 0.155f), new Vector3(0f, -1f, 0f), new Vector3(0.405f, -1f, 0.294f), new Vector3(0f, -1f, 0f), new Vector3(0.294f, -1f, 0.405f), new Vector3(0f, -1f, 0f), new Vector3(0.155f, -1f, 0.476f), new Vector3(0f, -1f, 0f), new Vector3(0f, -1f, 0.5f), new Vector3(0f, -1f, 0f), new Vector3(-0.155f, -1f, 0.476f), new Vector3(0f, -1f, 0f), new Vector3(-0.294f, -1f, 0.405f), new Vector3(0f, -1f, 0f), new Vector3(-0.405f, -1f, 0.294f), new Vector3(0f, -1f, 0f), new Vector3(-0.476f, -1f, 0.155f), new Vector3(0f, -1f, 0f), new Vector3(-0.5f, -1f, 0f), new Vector3(0f, -1f, 0f), new Vector3(-0.476f, 1f, -0.155f), new Vector3(0f, 1f, 0f), new Vector3(0f, 1f, 0f), new Vector3(-0.405f, 1f, -0.294f), new Vector3(0f, 1f, 0f), new Vector3(-0.294f, 1f, -0.405f), new Vector3(0f, 1f, 0f), new Vector3(-0.155f, 1f, -0.476f), new Vector3(0f, 1f, 0f), new Vector3(0f, 1f, -0.5f), new Vector3(0f, 1f, 0f), new Vector3(0.155f, 1f, -0.476f), new Vector3(0f, 1f, 0f), new Vector3(0.294f, 1f, -0.405f), new Vector3(0f, 1f, 0f), new Vector3(0.405f, 1f, -0.294f), new Vector3(0f, 1f, 0f), new Vector3(0.476f, 1f, -0.155f), new Vector3(0f, 1f, 0f), new Vector3(0.5f, 1f, 0f), new Vector3(0f, 1f, 0f), new Vector3(0.476f, 1f, 0.155f), new Vector3(0f, 1f, 0f), new Vector3(0.405f, 1f, 0.294f), new Vector3(0f, 1f, 0f), new Vector3(0.294f, 1f, 0.405f), new Vector3(0f, 1f, 0f), new Vector3(0.155f, 1f, 0.476f), new Vector3(0f, 1f, 0f), new Vector3(0f, 1f, 0.5f), new Vector3(0f, 1f, 0f), new Vector3(-0.155f, 1f, 0.476f), new Vector3(0f, 1f, 0f), new Vector3(-0.294f, 1f, 0.405f), new Vector3(0f, 1f, 0f), new Vector3(-0.405f, 1f, 0.294f), new Vector3(0f, 1f, 0f), new Vector3(-0.476f, 1f, 0.155f), new Vector3(0f, 1f, 0f), new Vector3(-0.5f, 1f, 0f)};
	

    [SerializeField]
    public class VectrosityCamInfo
    {
        public Camera camera;
        public string layer;

    }

    public Camera cam1;
    public string layer1;

    public Camera cam2;
    public string layer2;

    VectorLine vl;
    VectorLine vl2;

    // Use this for initialization
	void Start () {
        vl = new VectorLine("spheer", pts, 1);
        vl2 = new VectorLine("spheer", pts, 1);

        vl.drawTransform = this.transform;
        vl.layer = LayerMask.NameToLayer("pov_cam");

        vl2.drawTransform = this.transform;
        vl2.layer = LayerMask.NameToLayer("overall_cam");

	}
	
	// Update is called once per frame
	void Update () {

        VectorLine.SetCamera3D(cam1);
        vl.Draw3D();


        VectorLine.SetCamera3D(cam2);
        vl2.Draw3D();
	
	}
}
