﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;

public class RenderTextureScreen : MonoBehaviour {

    GameObject screenPlane;
    List<GameObject>textOverlays;
    RenderTexture renderTexture;

    public int nRows;
    public int nCols;

    //TODO : map cells to their cameras as well
    List<Camera> cellCameras;

    void Awake()
    {
        //screenPlane = this.transform.FindChild("plane");
    }

    public void setup(int nRows, int nCols)
    {
        screenPlane = this.transform.Find("plane").gameObject;
        this.nRows = nRows;
        this.nCols = nCols;
        //Place all the text overlays
        renderTexture = new RenderTexture(2048, 1024, 16);
        screenPlane.GetComponent<Renderer>().material.mainTexture = renderTexture;
        renderTexture.Create();
        cellCameras = new List<Camera>(nRows * nCols);

    }

    public void assignCamera  (Camera cam, int ri, int ci)
    {
        cam.rect = new Rect(
            (0.0f+ci)/nCols, (0.0f+ri)/nRows,
            1.0f/nCols, 1.0f/nRows);
        cam.targetTexture = this.renderTexture;
            
        //cellCameras[ri * nCols + ci] = cam;
    }


    public void assignCamera  (Camera cam, int i)
    {
        int ri = i / nCols;
        int ci = i % nCols;
        assignCamera(cam, ri, ci);
    }

    public Text getText(int ri, int rc)
    {
        return null;        
    }

    public void setup(int nCells)
    {
        int nRowsScreenCells = (int)Mathf.Floor(Mathf.Sqrt(nCells));
        int nColsScreenCells = (int)Mathf.Ceil(nCells /nRowsScreenCells);
        setup(nRowsScreenCells, nColsScreenCells);
    }

    public int nCells
    {
        get 
        {
            return -1;
        }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
